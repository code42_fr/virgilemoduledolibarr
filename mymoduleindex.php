<?php
/* Copyright (C) 2001-2005 Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2004-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2005-2012 Regis Houssin        <regis.houssin@capnetworks.com>
 * Copyright (C) 2015      Jean-François Ferry	<jfefe@aternatik.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/mymodule/template/mymoduleindex.php
 *	\ingroup    mymodule
 *	\brief      Home page of mymodule top menu
 */

		include 'controller_productlist.php';
		//Ce if => A SUPPRIMER (l'else doit d'executer si on a pas de canvas)
	 if (is_object($objcanvas) && $objcanvas->displayCanvasExists($action))
	 {
	 	$objcanvas->assign_values($action);       // Ceci doit contenir le code pour charger les données (Doit appeler LoadListDatas($limit, $offset, $sortfield, $sortorder))
	 	$objcanvas->display_canvas($action);  	  // Ce code doit afficher le template
	 }
	else {
		include 'model_product_request.php';

if ($resql) //Tout se lance si la requête fonctionne
{
	$num = $db->num_rows($resql);
	$i = 0;
	if ($num == 1 && ($sall || $snom || $sref || $sbarcode) && $action != 'list')
	{
		$objp = $db->fetch_object($resql);
		header("Location: card.php?id=".$objp->rowid);
		exit;
	}

	$helpurl='';
	if (isset($type)) // A SUPPRIMER si on s'en fout de helpurl
	{
		if ($type == 0)
			$helpurl='EN:Module_Products|FR:Module_Produits|ES:M&oacute;dulo_Productos';
		else if ($type == 1)
			$helpurl='EN:Module_Services_En|FR:Module_Services|ES:M&oacute;dulo_Servicios';
	}

	llxHeader('',$title,$helpurl,''); //Affichage du header dolibarr

	// Affiche un message de confirmation de la suppression.
	if (GETPOST('delprod'))	{
		setEventMessages($langs->trans("ProductDeleted", GETPOST('delprod')), null, 'mesgs');}

$param=''; //ENREGISTREMENT DES PARAMETRES DE L'URL.
if (! empty($contextpage) && $contextpage != $_SERVER["PHP_SELF"]) 	$param.='&contextpage='.$contextpage;
if ($limit > 0 && $limit != $conf->liste_limit) 										$param.='&limit='.$limit;
if ($search_categ > 0) 																							$param.="&amp;search_categ=".$search_categ;
if ($sref) 																													$param="&amp;sref=".$sref;
if ($search_ref_supplier) 																					$param="&amp;search_ref_supplier=".$search_ref_supplier;
if ($sbarcode) 																											$param.=($sbarcode?"&amp;sbarcode=".$sbarcode:"");
if ($snom) 																													$param.="&amp;snom=".$snom;
if ($sall) 																													$param.="&amp;sall=".$sall;
if ($tosell != '') 																									$param.="&amp;tosell=".$tosell;
if ($tobuy != '') 																									$param.="&amp;tobuy=".$tobuy;
if ($fourn_id) 																											$param.=($fourn_id?"&amp;fourn_id=".$fourn_id:"");
if ($seach_categ) 																									$param.=($search_categ?"&amp;search_categ=".$search_categ:"");
if ($type != '') 																										$param.='&amp;type='.urlencode($type);
if ($optioncss != '') 																							$param.='&optioncss='.$optioncss;
if ($search_tobatch) 																								$param="&amp;search_ref_supplier=".$search_ref_supplier;
if ($search_accountancy_code_sell) 																	$param="&amp;search_accountancy_code_sell=".$search_accountancy_code_sell;
if ($search_accountancy_code_buy) 																	$param="&amp;search_accountancy_code_buy=".$search_accountancy_code_buy;
// Ajout des parametres pour extrafield. A SUPPRIMER si pas de gestion des extrafields.
foreach ($search_array_options as $key => $val)
{
	$crit=$val;
	$tmpkey=preg_replace('/search_options_/','',$key);
	if ($val != '') $param.='&search_options_'.$tmpkey.'='.urlencode($val);
}

		//CHAMPS CACHÉS D'INPUT DE RECHERCHE
			print '<form action="'.$_SERVER["PHP_SELF"].'" method="post" name="formulaire">';
			if ($optioncss != '') print '<input type="hidden" name="optioncss" value="'.$optioncss.'">';
			print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
			print '<input type="hidden" name="formfilteraction" id="formfilteraction" value="list">';
			print '<input type="hidden" name="action" value="list">';
			print '<input type="hidden" name="sortfield" value="'.$sortfield.'">';
			print '<input type="hidden" name="sortorder" value="'.$sortorder.'">';
			print '<input type="hidden" name="type" value="'.$type.'">';

			/* PRINT_BARRE_LISTE :$texte : Titre de la barre:
			**										$page : Numéro de page
														$sortorder : ASC ou DESC en gros
														$limit : limite de lignes par tableau (set par defaut par dolibarr)
														$nbtotalofrecords : nombre de requetes sql fann_get_activation_steepness
														mymodule.png : picto d'illustration
														0 => picto / pas picto
			*/
			print_barre_liste($texte, $page, $_SERVER["PHP_SELF"], $param, $sortfield, $sortorder, '', $num, $nbtotalofrecords, 'mymodule.png@virgile', 0, '', '', $limit);

			//A SUPPRIMER : A voir ce que c'est a quoi ça sert
					if (! empty($catid))
    		{
    			print "<div id='ways'>";
    			$c = new Categorie($db);
    			$ways = $c->print_all_ways(' &gt; ','product/list.php');
    			print " &gt; ".$ways[0]."<br>\n";
    			print "</div><br>";
    		}

			//A SUPPRIMER : Canvas : SI pas de canvas supprimer
				if (! empty($canvas) && file_exists(DOL_DOCUMENT_ROOT.'/product/canvas/'.$canvas.'/actions_card_'.$canvas.'.class.php'))
	    	{
	    		$fieldlist = $object->field_list;
	    		$datas = $object->list_datas;
	    		$picto='title.png';
	    		$title_picto = img_picto('',$picto);
	    		$title_text = $title;

	    		// Default templates directory
	    		$template_dir = DOL_DOCUMENT_ROOT . '/product/canvas/'.$canvas.'/tpl/';
	    		// Check if a custom template is present
	    		if (file_exists(DOL_DOCUMENT_ROOT . '/theme/'.$conf->theme.'/tpl/product/'.$canvas.'/list.tpl.php'))
	    		{
	    			$template_dir = DOL_DOCUMENT_ROOT . '/theme/'.$conf->theme.'/tpl/product/'.$canvas.'/';
	    		}

	    		include $template_dir.'list.tpl.php';	// Include native PHP templates
	    	}
				else
				{
					if ($sall)
						{
							foreach($fieldstosearchall as $key => $val) $fieldstosearchall[$key]=$langs->trans($val);
							print $langs->trans("FilterOnInto", $sall) . join(', ',$fieldstosearchall);
						}

					//Filtrer les catégories
						$moreforfilter='';
						if (! empty($conf->categorie->enabled))
						{
							$moreforfilter.='<div class="divsearchfield">';
							$moreforfilter.=$langs->trans('Categories'). ': ';
							$moreforfilter.=$htmlother->select_categories(Categorie::TYPE_PRODUCT,$search_categ,'search_categ',1);
							$moreforfilter.='</div>';
					}
					if ($moreforfilter)
					{
							print '<div class="liste_titre liste_titre_bydiv centpercent">';
							print $moreforfilter;
							//A SUPPRIMER Si pas de gestion des hooks
									$parameters=array();
									$reshook=$hookmanager->executeHooks('printFieldPreListTitle',$parameters);    // Note that $action and $object may have been modified by hook
									print $hookmanager->resPrint;
							print '</div>';
					}

						//Gestion des champs selectionnés
							$varpage=empty($contextpage)?$_SERVER["PHP_SELF"]:$contextpage;
							$selectedfields=$form->multiSelectArrayWithCheckbox('selectedfields', $arrayfields, $varpage);	// This also change content of $arrayfields

						//Affichage des titres
					print '<table class="liste '.($moreforfilter?"listwithfilterbefore":"").'">';
					print '<tr class="liste_titre">';
					if (! empty($arrayfields['p.ref']['checked']))  print_liste_field_titre($arrayfields['p.ref']['label'], $_SERVER["PHP_SELF"],"p.ref","",$param,"",$sortfield,$sortorder);
					if (! empty($arrayfields['pfp.ref_fourn']['checked']))  print_liste_field_titre($arrayfields['pfp.ref_fourn']['label'], $_SERVER["PHP_SELF"],"pfp.ref_fourn","",$param,"",$sortfield,$sortorder);
					if (! empty($arrayfields['p.label']['checked']))  print_liste_field_titre($arrayfields['p.label']['label'], $_SERVER["PHP_SELF"],"p.label","",$param,"",$sortfield,$sortorder);
					if (! empty($arrayfields['p.barcode']['checked']))  print_liste_field_titre($arrayfields['p.barcode']['label'], $_SERVER["PHP_SELF"],"p.barcode","",$param,"",$sortfield,$sortorder);
					if (! empty($arrayfields['p.duration']['checked']))  print_liste_field_titre($arrayfields['p.duration']['label'], $_SERVER["PHP_SELF"],"p.duration","",$param,"",$sortfield,$sortorder);
					if (! empty($arrayfields['p.sellprice']['checked']))  print_liste_field_titre($arrayfields['p.sellprice']['label'], $_SERVER["PHP_SELF"],"","",$param,'align="right"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.minbuyprice']['checked']))  print_liste_field_titre($arrayfields['p.minbuyprice']['label'], $_SERVER["PHP_SELF"],"","",$param,'align="right"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.seuil_stock_alerte']['checked']))  print_liste_field_titre($arrayfields['p.seuil_stock_alerte']['label'], $_SERVER["PHP_SELF"],"p.seuil_stock_alerte","",$param,'align="right"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.desiredstock']['checked']))  print_liste_field_titre($arrayfields['p.desiredstock']['label'], $_SERVER["PHP_SELF"],"p.desiredstock","",$param,'align="right"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.stock']['checked']))  print_liste_field_titre($arrayfields['p.stock']['label'], $_SERVER["PHP_SELF"],"p.stock","",$param,'align="right"',$sortfield,$sortorder);
					if (! empty($arrayfields['stock_virtual']['checked']))  print_liste_field_titre($arrayfields['stock_virtual']['label'], $_SERVER["PHP_SELF"],"","",$param,'align="right"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.tobatch']['checked']))  print_liste_field_titre($arrayfields['p.tobatch']['label'], $_SERVER["PHP_SELF"],"p.tobatch","",$param,'align="center"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.accountancy_code_sell']['checked']))  print_liste_field_titre($arrayfields['p.accountancy_code_sell']['label'], $_SERVER["PHP_SELF"],"p.accountancy_code_sell","",$param,'',$sortfield,$sortorder);
					if (! empty($arrayfields['p.accountancy_code_buy']['checked']))  print_liste_field_titre($arrayfields['p.accountancy_code_buy']['label'], $_SERVER["PHP_SELF"],"p.accountancy_code_buy","",$param,'',$sortfield,$sortorder);

					//Extrafields : A SUPPRIMER SI PAS DE GESTION DES EXTRAFIELDS
							if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
							{
					 			foreach($extrafields->attribute_label as $key => $val)
					 				{
										if (! empty($arrayfields["ef.".$key]['checked']))
								 			{
									 			$align=$extrafields->getAlignFlag($key);
									 			print_liste_field_titre($extralabels[$key],$_SERVER["PHP_SELF"],"ef.".$key,"",$param,($align?'align="'.$align.'"':''),$sortfield,$sortorder);
								 			}
					 	 			}
							}

					// Hook fields : A SUPPRIMER SI PAS DE GESTION DES HOOKS
								$parameters=array('arrayfields'=>$arrayfields);
								$reshook=$hookmanager->executeHooks('printFieldListTitle',$parameters);    // Note that $action and $object may have been modified by hook
								print $hookmanager->resPrint;

					if (! empty($arrayfields['p.datec']['checked']))  print_liste_field_titre($arrayfields['p.datec']['label'],$_SERVER["PHP_SELF"],"p.datec","",$param,'align="center" class="nowrap"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.tms']['checked']))    print_liste_field_titre($arrayfields['p.tms']['label'],$_SERVER["PHP_SELF"],"p.tms","",$param,'align="center" class="nowrap"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.tosell']['checked'])) print_liste_field_titre($langs->trans("Status").' ('.$langs->trans("Sell").')',$_SERVER["PHP_SELF"],"p.tosell","",$param,'align="center"',$sortfield,$sortorder);
					if (! empty($arrayfields['p.tobuy']['checked']))  print_liste_field_titre($langs->trans("Status").' ('.$langs->trans("Buy").')',$_SERVER["PHP_SELF"],"p.tobuy","",$param,'align="center"',$sortfield,$sortorder);

					//Affichage d'un menu pour changer les $selected_fields
					print_liste_field_titre($selectedfields, $_SERVER["PHP_SELF"],"",'','','align="right"',$sortfield,$sortorder,'maxwidthsearch ');
							print "</tr>\n";

				include 'view_searchfield.php';
				include 'view_lines.php';

		    		$db->free($resql);
		    		print "</table>";
		    	}
		    	print '</form>';
		    }
		    else
		    {
		    	dol_print_error($db);
		    }
		}

		llxFooter();
		$db->close();
