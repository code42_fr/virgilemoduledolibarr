<?php

//Création des objects "produit"
$product_static=new Product($db);
$product_fourn =new ProductFournisseur($db);

$var  = true;

while ($i < min($num,$limit))
{
  $objp = $db->fetch_object($resql);

    //multilangue, a supprimer si pas de gestion du multilangue
            if (! empty($conf->global->MAIN_MULTILANGS)) // si l'option est active
            {
              //Requête SQL pour gérer multilang
                  $sql = "SELECT label";
                  $sql.= " FROM ".MAIN_DB_PREFIX."product_lang";
                  $sql.= " WHERE fk_product=".$objp->rowid;
                  $sql.= " AND lang='". $langs->getDefaultLang() ."'";
                  $sql.= " LIMIT 1";

              $result = $db->query($sql);
              if ($result)
              {
                $objtp = $db->fetch_object($result);
                if (! empty($objtp->label)) $objp->label = $objtp->label;
              }
            }

    //Initialisation des champs de l'objet avec les champs de la bdd
      $product_static->id         = $objp->rowid;
      $product_static->ref        = $objp->ref;
      $product_static->ref_fourn  = $objp->ref_supplier;
      $product_static->label      = $objp->label;
      $product_static->type       = $objp->fk_product_type;
      $product_static->status_buy = $objp->tobuy;
      $product_static->status     = $objp->tosell;
      $product_static->entity     = $objp->entity;

if (! empty($conf->stock->enabled) && $user->rights->stock->lire && $type != 1)	// To optimize call of load_stock
{
    if ($objp->fk_product_type != 1)    // Not a service
    {
        $product_static->load_stock('nobatch');             // Load stock_reel + stock_warehouse. This also call load_virtual_stock()
    }
}

  $var=!$var;
  print '<tr '.$bc[$var].'>';

  // Ref
  if (! empty($arrayfields['p.ref']['checked']))
  {
    print '<td class="nowrap">';
    print $product_static->getNomUrl(1,'',24);
    print "</td>\n";
  }
    // Ref supplier
  if (! empty($arrayfields['pfp.ref_fourn']['checked']))
  {
    print '<td class="nowrap">';
    print $product_static->getNomUrl(1,'',24);
    print "</td>\n";
  }
  // Label
  if (! empty($arrayfields['p.label']['checked']))
  {
    print '<td>'.dol_trunc($objp->label,40).'</td>';
  }

  // Barcode
  if (! empty($arrayfields['p.barcode']['checked']))
  {
    print '<td>'.$objp->barcode.'</td>';
  }

  // Duration
  if (! empty($arrayfields['p.duration']['checked']))
  {
    print '<td align="center">';
    if (preg_match('/([0-9]+)[a-z]/i',$objp->duration))
    {
      if (preg_match('/([0-9]+)y/i',$objp->duration,$regs)) print $regs[1].' '.$langs->trans("DurationYear");
      elseif (preg_match('/([0-9]+)m/i',$objp->duration,$regs)) print $regs[1].' '.$langs->trans("DurationMonth");
      elseif (preg_match('/([0-9]+)w/i',$objp->duration,$regs)) print $regs[1].' '.$langs->trans("DurationWeek");
      elseif (preg_match('/([0-9]+)d/i',$objp->duration,$regs)) print $regs[1].' '.$langs->trans("DurationDay");
      //elseif (preg_match('/([0-9]+)h/i',$objp->duration,$regs)) print $regs[1].' '.$langs->trans("DurationHour");
      else print $objp->duration;
    }
    print '</td>';
  }

  // Sell price
  if (! empty($arrayfields['p.sellprice']['checked']))
  {
      print '<td align="right">';
      if ($objp->tosell)
      {
        if ($objp->price_base_type == 'TTC') print price($objp->price_ttc).' '.$langs->trans("TTC");
        else print price($objp->price).' '.$langs->trans("HT");
      }
      print '</td>';
  }

  // Better buy price
  if (! empty($arrayfields['p.minbuyprice']['checked']))
  {
      print  '<td align="right">';
      if ($objp->tobuy && $objp->minsellprice != '')
      {
      //print price($objp->minsellprice).' '.$langs->trans("HT");
      if ($product_fourn->find_min_price_product_fournisseur($objp->rowid) > 0)
      {
        if ($product_fourn->product_fourn_price_id > 0)
        {
          if (! empty($conf->fournisseur->enabled) && $user->rights->fournisseur->lire)
          {
            $htmltext=$product_fourn->display_price_product_fournisseur(1, 1, 0, 1);
            print $form->textwithpicto(price($product_fourn->fourn_unitprice).' '.$langs->trans("HT"),$htmltext);
          }
          else print price($product_fourn->fourn_unitprice).' '.$langs->trans("HT");
        }
      }
      }
      print '</td>';
  }

    // Limit alert
    if (! empty($arrayfields['p.seuil_stock_alerte']['checked']))
    {
            print '<td align="right">';
    if ($objp->fk_product_type != 1)
    {
                print $objp->seuil_stock_alerte;
    }
    print '</td>';
    }
  // Desired stock
    if (! empty($arrayfields['p.desiredstock']['checked']))
    {
            print '<td align="right">';
    if ($objp->fk_product_type != 1)
    {
                print $objp->desiredstock;
    }
    print '</td>';
    }
// Stock
    if (! empty($arrayfields['p.stock']['checked']))
    {
    print '<td align="right">';
    if ($objp->fk_product_type != 1)
    {
      if ($objp->seuil_stock_alerte != '' && $product_static->stock_reel < (float) $objp->seuil_stock_alerte) print img_warning($langs->trans("StockTooLow")).' ';
        print $product_static->stock_reel;
    }
    print '</td>';
  }
  // Stock
    if (! empty($arrayfields['stock_virtual']['checked']))
    {
    print '<td align="right">';
    if ($objp->fk_product_type != 1)
    {
      if ($objp->seuil_stock_alerte != '' && $product_static->stock_theorique < (float) $objp->seuil_stock_alerte) print img_warning($langs->trans("StockTooLow")).' ';
        print $product_static->stock_theorique;
    }
    print '</td>';
  }
  // Lot/Serial
    if (! empty($arrayfields['p.tobatch']['checked']))
    {
            print '<td align="center">';
    print yn($objp->tobatch);
    print '</td>';
    }
  // Accountancy code sell
    if (! empty($arrayfields['p.accountancy_code_sell']['checked'])) print '<td>'.$objp->accountancy_code_sell.'</td>';
  // Accountancy code sell
    if (! empty($arrayfields['p.accountancy_code_buy']['checked'])) print '<td>'.$objp->accountancy_code_buy.'</td>';
    // Extra fields
if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
{
   foreach($extrafields->attribute_label as $key => $val)
   {
    if (! empty($arrayfields["ef.".$key]['checked']))
    {
      print '<td';
      $align=$extrafields->getAlignFlag($key);
      if ($align) print ' align="'.$align.'"';
      print '>';
      $tmpkey='options_'.$key;
      print $extrafields->showOutputField($key, $objp->$tmpkey, '', 1);
      print '</td>';
    }
   }
}
    // Fields from hook
  $parameters=array('arrayfields'=>$arrayfields, 'obj'=>$obj);
$reshook=$hookmanager->executeHooks('printFieldListValue',$parameters);    // Note that $action and $object may have been modified by hook
    print $hookmanager->resPrint;
    // Date creation
    if (! empty($arrayfields['p.datec']['checked']))
    {
        print '<td align="center">';
        print dol_print_date($objp->date_creation, 'dayhour');
        print '</td>';
    }
    // Date modification
    if (! empty($arrayfields['p.tms']['checked']))
    {
        print '<td align="center">';
        print dol_print_date($objp->date_update, 'dayhour');
        print '</td>';
    }

        // Status (to sell)
    if (! empty($arrayfields['p.tosell']['checked']))
    {
          print '<td align="center" nowrap="nowrap">';
          if (! empty($conf->use_javascript_ajax) && $user->rights->produit->creer && ! empty($conf->global->MAIN_DIRECT_STATUS_UPDATE)) {
              print ajax_object_onoff($product_static, 'status', 'tosell', 'ProductStatusOnSell', 'ProductStatusNotOnSell');
          } else {
              print $product_static->LibStatut($objp->tosell,5,0);
          }
          print '</td>';
    }
        // Status (to buy)
    if (! empty($arrayfields['p.tobuy']['checked']))
    {
      print '<td align="center" nowrap="nowrap">';
          if (! empty($conf->use_javascript_ajax) && $user->rights->produit->creer && ! empty($conf->global->MAIN_DIRECT_STATUS_UPDATE)) {
              print ajax_object_onoff($product_static, 'status_buy', 'tobuy', 'ProductStatusOnBuy', 'ProductStatusNotOnBuy');
          } else {
              print $product_static->LibStatut($objp->tobuy,5,1);
          }
          print '</td>';
    }
    // Action
        print '<td>&nbsp;</td>';

        print "</tr>\n";
  $i++;
}

print_barre_liste('', $page, $_SERVER["PHP_SELF"], $param, $sortfield, $sortorder, '', $num, $nbtotalofrecords, '', '', '', 'paginationatbottom');
