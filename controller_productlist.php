<?php // Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../main.inc.php")) $res=@include("../main.inc.php");
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");


//Dépendances
	//Dépendances Fonctions Formulaires
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
	//Dépendances product_list
require_once DOL_DOCUMENT_ROOT.'/product/class/product.class.php';
require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.product.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formother.class.php';
if (! empty($conf->categorie->enabled))
	require_once DOL_DOCUMENT_ROOT.'/categories/class/categorie.class.php';
	//Dépendances pour la liste des actions

//Modules langues
$langs->load("products");
$langs->load("stocks");
$langs->load("suppliers");
$langs->load("companies");
if (! empty($conf->productbatch->enabled)) $langs->load("productbatch");
/*	$langs->load('virgile@virgile');
**	Aucun fichier de traduction créée pour le moment
*/

//CHAMPS GETPOST

$action = GETPOST('action');
$sref=GETPOST("sref");
$sbarcode=GETPOST("sbarcode");
$snom=GETPOST("snom");
$sall=GETPOST("sall");
$type=GETPOST("type","int");
$search_sale = GETPOST("search_sale");
$search_categ = GETPOST("search_categ",'int');
$tosell = GETPOST("tosell", 'int');
$tobuy = GETPOST("tobuy", 'int');
$fourn_id = GETPOST("fourn_id",'int');
$catid = GETPOST('catid','int');
$search_tobatch = GETPOST("search_tobatch",'int');
$search_accountancy_code_sell = GETPOST("search_accountancy_code_sell",'alpha');
$search_accountancy_code_buy = GETPOST("search_accountancy_code_buy",'alpha');
$optioncss = GETPOST('optioncss','alpha');
$limit = GETPOST("limit")?GETPOST("limit","int"):$conf->liste_limit;
$sortfield = GETPOST("sortfield",'alpha');
$sortorder = GETPOST("sortorder",'alpha');
$page = GETPOST("page",'int');


	//Gestion des variables multi-pages
		if ($page == -1)
		{
			$page = 0;
		}
		if (GETPOST("button_removefilter_x") || GETPOST("button_removefilter.x") || GETPOST("button_removefilter")) // Both test are required to be compatible with all browsers
		{
			$sall="";
			$sref="";
			$snom="";
			$sbarcode="";
			$search_categ=0;
			$tosell="";
			$tobuy="";
			$search_tobatch='';
			$search_accountancy_code_sell='';
			$search_accountancy_code_buy='';
			$search_array_options=array();
		}
$offset = $limit * $page; //Limit : Variable dolibarr donnant la limite de lignes par page.
$pageprev = $page - 1;
$pagenext = $page + 1;
if (! $sortfield) $sortfield="p.ref"; // Si aucun critère de tri selectionné, on tri par ordre alphabetique de reference
if (! $sortorder) $sortorder="ASC"; // ASC OU DESC => ASC par defaut

// On détermine le contexte : Produit, ou service? $search_type set en fonction
$contextpage=GETPOST('contextpage','aZ')?GETPOST('contextpage','aZ'):'productservicelist';
if ($type === '1') { $contextpage='servicelist'; if ($search_type=='') $search_type='1'; }
if ($type === '0') { $contextpage='productlist'; if ($search_type=='') $search_type='0'; }

// Gestion des hooks, A SUPPRIMER !
$hookmanager->initHooks(array($contextpage));
$extrafields = new ExtraFields($db);
$form=new Form($db);

// Prends les labels optionels, A SUPPRIMER !
$extralabels = $extrafields->fetch_name_optionals_label('product');
$search_array_options=$extrafields->getOptionalsFromPost($extralabels,'','search_');

//Si l'user n'effectue pas d'action, on liste juste.
if (empty($action)) $action='list';

// object canvas, A SUPPRIMER !
$canvas=GETPOST("canvas");
$objcanvas=null;
if (! empty($canvas))
{
    require_once DOL_DOCUMENT_ROOT.'/core/class/canvas.class.php';
    $objcanvas = new Canvas($db,$action);
    $objcanvas->getCanvas('product','list',$canvas);
}

// Security check A SUPPRIMER !
if ($type=='0') $result=restrictedArea($user,'produit','','','','','',$objcanvas);
else if ($type=='1') $result=restrictedArea($user,'service','','','','','',$objcanvas);
else $result=restrictedArea($user,'produit|service','','','','','',$objcanvas);

//Je sais pas a quoi ça sert, a supprimer si ça fonctionne sans...
$virtualdiffersfromphysical=0;
if (! empty($conf->global->STOCK_CALCULATE_ON_SHIPMENT) || ! empty($conf->global->STOCK_CALCULATE_ON_SUPPLIER_DISPATCH_ORDER))
{
    $virtualdiffersfromphysical=1;		// According to increase/decrease stock options, virtual and physical stock may differs.
}

//Liste des champs recherchés quand on selectionne l'option "rechercher partout"
//a supprimer si la feature n'est pas integrée
$fieldstosearchall = array(
'p.ref'						=>	"Ref",
'pfp.ref_fourn'		=>	"RefSupplier",
'p.label'					=>	"ProductLabel",
'p.description'		=>	"Description",
"p.note"					=>	"Note",
);

// Gestion Dolibarr des fichiers multilangues.
// A supprimer si non-necessaire pour lancer $langs->trans
if (! empty($conf->global->MAIN_MULTILANGS))
{
	$fieldstosearchall['pl.label']='ProductLabelTranslated';
	$fieldstosearchall['pl.description']='ProductDescriptionTranslated';
	$fieldstosearchall['pl.note']='ProductNoteTranslated';
}
if (! empty($conf->barcode->enabled)) {
	$fieldstosearchall['p.barcode']='Gencod';
}

if (empty($conf->global->PRODUIT_MULTIPRICES))
{
	$titlesellprice=$langs->trans("SellingPrice");
	if (! empty($conf->global->PRODUIT_CUSTOMER_PRICES))
	{
		$titlesellprice=$form->textwithpicto($langs->trans("SellingPrice"), $langs->trans("DefaultPriceRealPriceMayDependOnCustomer"));
	}
}


//Définition des champs : $langs=>trans permet d'accéder aux champs traduits dans la base de données CERTAINS CHAMPS A SUPPRIMER

$arrayfields=array(
'p.ref'=>														array('label'=>$langs->trans("Ref"), 'checked'=>1),
'p.label'=>													array('label'=>$langs->trans("Label"), 'checked'=>1),
'p.barcode'=>												array('label'=>$langs->trans("Gencod"), 'checked'=>($contextpage != 'servicelist'), 'enabled'=>(! empty($conf->barcode->enabled))),
'p.duration'=>											array('label'=>$langs->trans("Duration"), 'checked'=>($contextpage != 'productlist'), 'enabled'=>(! empty($conf->service->enabled))),
'p.sellprice'=>											array('label'=>$langs->trans("SellingPrice"), 'checked'=>1, 'enabled'=>empty($conf->global->PRODUIT_MULTIPRICES)),
'p.minbuyprice'=>										array('label'=>$langs->trans("BuyingPriceMinShort"), 'checked'=>1, 'enabled'=>(! empty($user->rights->fournisseur->lire))),
'p.seuil_stock_alerte'=>						array('label'=>$langs->trans("StockLimit"), 'checked'=>0, 'enabled'=>(! empty($conf->stock->enabled) && $user->rights->stock->lire && $contextpage != 'service')),
'p.desiredstock'=>									array('label'=>$langs->trans("DesiredStock"), 'checked'=>1, 'enabled'=>(! empty($conf->stock->enabled) && $user->rights->stock->lire && $contextpage != 'service')),
'p.stock'=>													array('label'=>$langs->trans("PhysicalStock"), 'checked'=>1, 'enabled'=>(! empty($conf->stock->enabled) && $user->rights->stock->lire && $contextpage != 'service')),
'stock_virtual'=>										array('label'=>$langs->trans("VirtualStock"), 'checked'=>1, 'enabled'=>(! empty($conf->stock->enabled) && $user->rights->stock->lire && $contextpage != 'service' && $virtualdiffersfromphysical)),
'p.tobatch'=>												array('label'=>$langs->trans("ManageLotSerial"), 'checked'=>0, 'enabled'=>(! empty($conf->productbatch->enabled))),
'p.accountancy_code_sell'=>					array('label'=>$langs->trans("ProductAccountancySellCode"), 'checked'=>0),
'p.accountancy_code_buy'=>					array('label'=>$langs->trans("ProductAccountancyBuyCode"), 'checked'=>0),
'p.datec'=>													array('label'=>$langs->trans("DateCreation"), 'checked'=>0, 'position'=>500),
'p.tms'=>														array('label'=>$langs->trans("DateModificationShort"), 'checked'=>0, 'position'=>500),
'p.tosell'=>												array('label'=>$langs->trans("Status").' ('.$langs->trans("Sell").')', 'checked'=>1, 'position'=>1000),
'p.tobuy'=>													array('label'=>$langs->trans("Status").' ('.$langs->trans("Purchases").')', 'checked'=>1, 'position'=>1000)
);

// Extra fields A SUPPRIMER
if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
{
   foreach($extrafields->attribute_label as $key => $val)
   {
       $arrayfields["ef.".$key]=array('label'=>$extrafields->attribute_label[$key], 'checked'=>$extrafields->attribute_list[$key], 'position'=>$extrafields->attribute_pos[$key]);
   }
}

/*
 * LISTE DES ACTIONS
 */

	include DOL_DOCUMENT_ROOT.'/core/actions_changeselectedfields.inc.php';

 // Si on appuie sur le bouton pour clear les champs / Tests différents pour navigateurs différents
 if (GETPOST("button_removefilter_x") || GETPOST("button_removefilter.x") || GETPOST("button_removefilter"))
 {
 	$sall="";
 	$sref="";
 	$snom="";
 	$sbarcode="";
 	$search_categ=0;
 	$tosell="";
 	$tobuy="";
 	$search_tobatch='';
 	$search_accountancy_code_sell='';
 	$search_accountancy_code_buy='';
 	$search_array_options=array();
 }

$action=GETPOST('action', 'alpha');

/*
 * Vue
 */

 $htmlother=new FormOther($db);

 $title=$langs->trans("ProductsAndServices");

 //A DEPLACER dans l'initialisation si possible
 if (isset($type))
 {
   if ($type == 1)
       $texte = $langs->trans("Services");
   else
        $texte = $langs->trans("Products");
 }
 else
   $texte = $langs->trans("ProductsAndServices");
