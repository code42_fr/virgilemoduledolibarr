<?php


print '<tr class="liste_titre">';

//EXEMPLE avec Ref :
  if (! empty($arrayfields['p.ref']['checked'])) //Si p.ref est selectionné
  {
    print '<td class="liste_titre" align="left">';
    print '<input class="flat" type="text" name="sref" size="8" value="'.dol_escape_htmltag($sref).'">';
    print '</td>';
  }
  if (! empty($arrayfields['pfp.ref_fourn']['checked']))
{
  print '<td class="liste_titre" align="left">';
  print '<input class="flat" type="text" name="search_ref_supplier" size="8" value="'.dol_escape_htmltag($search_ref_supplier).'">';
  print '</td>';
}
if (! empty($arrayfields['p.label']['checked']))
{
  print '<td class="liste_titre" align="left">';
  print '<input class="flat" type="text" name="snom" size="12" value="'.dol_escape_htmltag($snom).'">';
  print '</td>';
}
// Barcode
if (! empty($arrayfields['p.barcode']['checked']))
{
  print '<td class="liste_titre">';
  print '<input class="flat" type="text" name="sbarcode" size="6" value="'.dol_escape_htmltag($sbarcode).'">';
  print '</td>';
}
// Duration
if (! empty($arrayfields['p.duration']['checked']))
{
  print '<td class="liste_titre">';
  print '&nbsp;';
  print '</td>';
}
// Sell price
if (! empty($arrayfields['p.sellprice']['checked']))
    {
    print '<td class="liste_titre" align="right">';
    print '</td>';
    }
// Minimum buying Price
if (! empty($arrayfields['p.minbuyprice']['checked']))
{
  print '<td class="liste_titre">';
  print '&nbsp;';
  print '</td>';
}
  // Limit for alert
if (! empty($arrayfields['p.seuil_stock_alerte']['checked']))
{
  print '<td class="liste_titre">';
  print '&nbsp;';
  print '</td>';
}
// Desired stock
if (! empty($arrayfields['p.desiredstock']['checked']))
{
  print '<td class="liste_titre">';
  print '&nbsp;';
  print '</td>';
}
// Stock
if (! empty($arrayfields['p.stock']['checked'])) print '<td class="liste_titre">&nbsp;</td>';
// Stock
if (! empty($arrayfields['stock_virtual']['checked'])) print '<td class="liste_titre">&nbsp;</td>';
// To batch
if (! empty($arrayfields['p.tobatch']['checked'])) print '<td class="liste_titre center">'.$form->selectyesno($search_tobatch, '', '', '', 1).'</td>';
// Accountancy code sell
if (! empty($arrayfields['p.accountancy_code_sell']['checked'])) print '<td class="liste_titre"><input class="flat" type="text" name="search_accountancy_code_sell" size="6" value="'.dol_escape_htmltag($search_accountancy_code_sell).'"></td>';
  // Accountancy code sell
if (! empty($arrayfields['p.accountancy_code_buy']['checked'])) print '<td class="liste_titre"><input class="flat" type="text" name="search_accountancy_code_buy" size="6" value="'.dol_escape_htmltag($search_accountancy_code_buy).'"></td>';


      // Extra fields A SUPPRIMER si pas de gestion des extrafields
          if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label))
          {
             foreach($extrafields->attribute_label as $key => $val)
             {
              if (! empty($arrayfields["ef.".$key]['checked'])) print '<td class="liste_titre"></td>';
             }
          }


      // Champs Hook, A SUPPRIMER si pas de gestion des hooks
          $parameters=array('arrayfields'=>$arrayfields);
          $reshook=$hookmanager->executeHooks('printFieldListOption',$parameters);    // Note that $action and $object may have been modified by hook
          print $hookmanager->resPrint;

    // Date creation
        if (! empty($arrayfields['p.datec']['checked']))
        {
            print '<td class="liste_titre">';
            print '</td>';
        }

    // Date modification
        if (! empty($arrayfields['p.tms']['checked']))
        {
            print '<td class="liste_titre">';
            print '</td>';
        }

    //Exemple : champ avec un choix multiple
      if (! empty($arrayfields['p.tosell']['checked']))
      {
            print '<td class="liste_titre" align="center">';
              //$form->selectarray + nom de la selection + traduction langue = panneau de choix multiples
                print $form->selectarray('tosell', array('0'=>$langs->trans('ProductStatusNotOnSellShort'),'1'=>$langs->trans('ProductStatusOnSellShort')),$tosell,1);
            print '</td >';
      }
    if (! empty($arrayfields['p.tobuy']['checked']))
    {
          print '<td class="liste_titre" align="center">';
          print $form->selectarray('tobuy', array('0'=>$langs->trans('ProductStatusNotOnBuyShort'),'1'=>$langs->trans('ProductStatusOnBuyShort')),$tobuy,1);
          print '</td>';
    }
    print '<td class="liste_titre" align="right">';
    //Affichage de la loupe
    $searchpitco=$form->showFilterAndCheckAddButtons(0);
    print $searchpitco;
    print '</td>';

print '</tr>';
