<?php 
//REQUETE SQL
    //REQUETE DE base
    $sql = 'SELECT DISTINCT p.rowid, p.ref, p.label, p.barcode, p.price, p.price_ttc, p.price_base_type, p.entity,';
    $sql.= ' p.fk_product_type, p.duration, p.tosell, p.tobuy, p.seuil_stock_alerte, p.desiredstock,';
    $sql.= ' p.tobatch, p.accountancy_code_sell, p.accountancy_code_buy,';
    $sql.= ' p.datec as date_creation, p.tms as date_update,';
    $sql.= ' MIN(pfp.unitprice) as minsellprice';

    // Ajout des champs Extrafields, A SUPPRIMER si possible
    foreach ($extrafields->attribute_label as $key => $val) $sql.=($extrafields->attribute_type[$key] != 'separate' ? ",ef.".$key.' as options_'.$key : '');

    // Ajout des champs hooks (a supprimer si hooks supprimés)
    $parameters=array();
    $reshook=$hookmanager->executeHooks('printFieldListSelect',$parameters);    // Note that $action and $object may have been modified by hook
    $sql.=$hookmanager->resPrint;

    $sql.= ' FROM '.MAIN_DB_PREFIX.'product as p';

    //Extrafields => A SUPPRIMER
    if (is_array($extrafields->attribute_label) && count($extrafields->attribute_label)) $sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product_extrafields as ef on (p.rowid = ef.fk_object)";
      if (! empty($search_categ) || ! empty($catid)) $sql.= ' LEFT JOIN '.MAIN_DB_PREFIX."categorie_product as cp ON p.rowid = cp.fk_product"; // We'll need this table joined to the select in order to filter by categ
      $sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product_fournisseur_price as pfp ON p.rowid = pfp.fk_product";

    //Gestion des langues, a supprimer si pas de gestion des langues
    if (! empty($conf->global->MAIN_MULTILANGS)) $sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product_lang as pl ON pl.fk_product = p.rowid AND pl.lang = '".$langs->getDefaultLang() ."'";
    $sql.= ' WHERE p.entity IN ('.getEntity('product', 1).')';
    if ($sall) $sql .= natural_search(array_keys($fieldstosearchall), $sall);

    // Si le type est différent de 1, on montre tous les profuits (0, 2, 3)
    if (dol_strlen($type))
    {
      if ($type == 1) $sql.= " AND p.fk_product_type = '1'";
      else 						$sql.= " AND p.fk_product_type <> '1'";
    }

    //Gestion des foreign keys avec la fonction dolibarr ($db->escape) et la fonction natural_search
    if ($sref)     $sql .= natural_search('p.ref', $sref);
    if ($snom)     $sql .= natural_search('p.label', $snom);
    if ($sbarcode) $sql .= natural_search('p.barcode', $sbarcode);
    if (isset($tosell) && dol_strlen($tosell) > 0  && $tosell!=-1) $sql.= " AND p.tosell = ".$db->escape($tosell);
    if (isset($tobuy) && dol_strlen($tobuy) > 0  && $tobuy!=-1)   $sql.= " AND p.tobuy = ".$db->escape($tobuy);
    if (dol_strlen($canvas) > 0)                    $sql.= " AND p.canvas = '".$db->escape($canvas)."'"; //CANVAS : A SUPPRIMER SI PAS DE CANVAS
    if ($catid > 0)    $sql.= " AND cp.fk_categorie = ".$catid;
    if ($catid == -2)  $sql.= " AND cp.fk_categorie IS NULL";
    if ($search_categ > 0)   $sql.= " AND cp.fk_categorie = ".$db->escape($search_categ);
    if ($search_categ == -2) $sql.= " AND cp.fk_categorie IS NULL";
    if ($fourn_id > 0) $sql.= " AND pfp.fk_soc = ".$fourn_id;
    if ($search_tobatch != '' && $search_tobatch >= 0)   $sql.= " AND p.tobatch = ".$db->escape($search_tobatch);
    if ($search_accountancy_code_sell)   $sql.= natural_search('p.accountancy_code_sell', $search_accountancy_code_sell);
    if ($search_accountancy_code_sell)   $sql.= natural_search('p.accountancy_code_buy', $search_accountancy_code_buy);

    // Ajout du WHERE pour les extra fields, A SUPPRIMER si pas de gestion des extrafields
    foreach ($search_array_options as $key => $val)
      {
        $crit=$val;
        $tmpkey=preg_replace('/search_options_/','',$key);
        $typ=$extrafields->attribute_type[$tmpkey];
        $mode=0;
        if (in_array($typ, array('int','double'))) $mode=1;    // Search on a numeric
        if ($val && ( ($crit != '' && ! in_array($typ, array('select'))) || ! empty($crit)))
          {
            $sql .= natural_search('ef.'.$tmpkey, $crit, $mode);
          }
      }

    // Ajout du WHERE pour les hooks, a supprimer si pas de gestion des hooks
    $parameters=array();
    $reshook=$hookmanager->executeHooks('printFieldListWhere',$parameters);    // Note that $action and $object may have been modified by hook
    $sql.=$hookmanager->resPrint;
    $sql.= " GROUP BY p.rowid, p.ref, p.label, p.barcode, p.price, p.price_ttc, p.price_base_type,";
    $sql.= " p.fk_product_type, p.duration, p.tosell, p.tobuy, p.seuil_stock_alerte, p.desiredstock,";
    $sql.= ' p.datec, p.tms, p.entity, p.tobatch, p.accountancy_code_sell, p.accountancy_code_buy';

    // Ajout des champs Extrafields, A SUPPRIMER si pas de gestion des extrafields
      foreach ($extrafields->attribute_label as $key => $val) $sql.=($extrafields->attribute_type[$key] != 'separate' ? ",ef.".$key : '');

    // Ajout des champs HOOKS, A SUPPRIMER si pas de gestion des hooks
    $parameters=array();
    $reshook=$hookmanager->executeHooks('printFieldSelect',$parameters);    // Note that $action and $object may have been modified by hook
    $sql.=$hookmanager->resPrint;

    //Finalisation de la requete, stockage du resultat
    $sql.= $db->order($sortfield,$sortorder);
    $nbtotalofrecords = 0;
    if (empty($conf->global->MAIN_DISABLE_FULL_SCANLIST)) //on set result et nbtotalofrecords si dolibarr le fait pas tout seul
    {
      $result = $db->query($sql);
      $nbtotalofrecords = $db->num_rows($result);
    }
    $sql.= $db->plimit($limit + 1, $offset);
    $resql = $db->query($sql);

  //FIN DE LA REQUETE
